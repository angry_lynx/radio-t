package ru.iristech.radiot;

import android.graphics.Bitmap;
import android.util.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Item {
    private static final String TAG = "Item";
    private String mTitle;
    private String mPictureUrl;
    private String mSubjects;
    private Date mDate;
    private Bitmap mPicture;
    private String mMp3Url;

    public Item(String mTitle, String subjects, String description, String pubDate, String mp3Url) {
        this.mTitle = mTitle;
        this.mMp3Url = mp3Url;
        Pattern p = Pattern.compile("([^0-9]+)([0-9]{1,3})");
        Matcher m = p.matcher(mTitle);
        if (m.matches()) {
            mTitle = m.group(2);
        }
        mSubjects = subjects;
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

        try {
            mDate = format.parse(pubDate);
        } catch (ParseException e) {
            Log.e(TAG, "Ошибка в Item.Item() при парсинге даты: " + e);
        }

        Document doc = Jsoup.parse(description);
        Elements pic = doc.getElementsByTag("img");
        mPictureUrl = pic.get(0).attr("src");
    }

    public String getTitle() {
        return mTitle;
    }
    public String getSubjects() {
        return mSubjects;
    }

    public String getPictureUrl() {
        return mPictureUrl;
    }

    public Date getDate() {
        return mDate;
    }

    public Bitmap getPicture() {
        return mPicture;
    }

    public void setPicture(Bitmap mPicture) {
        this.mPicture = mPicture;
    }

    public String getMp3Url() {
        return mMp3Url;
    }

}




















