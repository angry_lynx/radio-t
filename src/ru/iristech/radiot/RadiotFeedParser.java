package ru.iristech.radiot;

import android.util.Log;
import android.util.Xml;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RadiotFeedParser extends Thread {
    private static final String TAG = "RadiotFeedParser";

    List<Item> mItems;

    @Override
    public void run() {
        try {
            InputStream xmlStream = getFeed(Settings.RADIOT_FEED_URL);
            mItems = parseFeed(xmlStream);
        } catch (IOException e) {
            Log.e(TAG, "ОШИБКА -> " + e);
        } catch (XmlPullParserException e) {
            Log.e(TAG, "ОШИБКА -> " + e);
        }
    }

    private InputStream getFeed(String url) throws IOException {
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
        HttpConnectionParams.setSoTimeout(httpParameters, 4000);
        HttpClient httpclient = new DefaultHttpClient(httpParameters);
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        return entity.getContent();
    }

    private List<Item> parseFeed(InputStream xmlStream) throws XmlPullParserException, IOException {

        List<Item> items = new ArrayList<>();
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(xmlStream, null);
        int eventType = parser.getEventType();
        boolean inItemTag = false;
        String title = null;
        String subjects = null;
        String description = null;
        String pubDate = null;
        String mp3Url = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.getName().equals("item")) {
                    inItemTag = true;
                } else {
                    switch (parser.getName()) {
                        case "title": title = parser.nextText();
                            break;
                        case "summary": subjects = parser.nextText();
                            break;
                        case "description": description = parser.nextText();
                            break;
                        case "pubDate": pubDate = parser.nextText();
                            break;
                        case "content": mp3Url = parser.getAttributeValue(0);
                            break;

                    }
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("item")) {
                    inItemTag = false;

                    items.add(new Item(title, subjects, description, pubDate, mp3Url));
                }
            }

            eventType = parser.next();
        }
        return items;
    }

    public List<Item> getItems() {
        return mItems;
    }
}



































