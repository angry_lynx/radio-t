package ru.iristech.radiot;

import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;

import android.net.Uri;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;

import java.io.IOException;


public class PlayerDialog extends Dialog implements MediaPlayer.OnBufferingUpdateListener, View.OnTouchListener {

    private static final String TAG = "PlayerDialog";
    private final Item item;
    private PlayerThread handlerThread;
    private SeekBar songSeekBar;
    private ImageButton playPauseButton;
    private final MediaPlayer player;
    private float mediaFileLengthInMilliseconds;
    private final Handler handlerProgressUpdater = new Handler();

    Runnable notificationProgressUpdater = new Runnable() {
        public void run() {
            primarySeekBarProgressUpdater();
        }
    };


    protected PlayerDialog(Context context, boolean cancelable, OnCancelListener cancelListener, Item item) {
        super(context, cancelable, cancelListener);
        this.item = item;
        player = new MediaPlayer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_dialog);
        setTitle(item.getTitle());
        songSeekBar = (SeekBar)findViewById(R.id.songSeekBar);
        playPauseButton = (ImageButton)findViewById(R.id.playPauseButton);
        songSeekBar.setMax(99);
        playPauseButton.setOnTouchListener(this);
        songSeekBar.setOnTouchListener(this);
        ((ImageButton)findViewById(R.id.backwardButton)).setOnTouchListener(this);
        ((ImageButton)findViewById(R.id.forwardButton)).setOnTouchListener(this);

    }

    @Override
    public void show() {
        super.show();
        player.setOnBufferingUpdateListener(this);
        try {
            handlerThread = new PlayerThread(player);
            mediaFileLengthInMilliseconds = player.getDuration();
            handlerThread.start();
        } catch (IOException e) {
            Log.e(TAG, "Немогу воспроизвести файл " + item + ". Причина: " + e);
            if (handlerThread != null) {
                handlerThread.interrupt();
            }
            hide();
        }
        primarySeekBarProgressUpdater();
    }

    @Override
    public void cancel() {
        handlerThread.interrupt();
        handlerThread = null;
        handlerProgressUpdater.removeCallbacks(notificationProgressUpdater);
        super.cancel();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        songSeekBar.setSecondaryProgress(percent);
    }

    private void primarySeekBarProgressUpdater() {
        float currentPosition = player.getCurrentPosition();
        int progress = (int)((currentPosition/mediaFileLengthInMilliseconds)*100);
        songSeekBar.setProgress(progress);
        Log.d(TAG, String.valueOf(progress));
        if (player.isPlaying()) {
            handlerProgressUpdater.postDelayed(notificationProgressUpdater, 5000);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (v.getId() == R.id.playPauseButton && event.getAction() == MotionEvent.ACTION_UP) {
            ImageButton btn = (ImageButton) v;
            if (player.isPlaying()) {
                btn.setImageResource(R.drawable.play);
                player.pause();

            } else {
                btn.setImageResource(R.drawable.pause);
                player.start();
            }
        } else if (v.getId() == R.id.songSeekBar) {
            if (player.isPlaying()) {
                SeekBar sb = (SeekBar) v;
                int playPositionInMillisecconds = (int) ((mediaFileLengthInMilliseconds / 100) *  (float)sb.getProgress());
                player.seekTo(playPositionInMillisecconds);
            }
        } else if (v.getId() == R.id.forwardButton && player.isPlaying()) {
            player.seekTo(player.getCurrentPosition() + 10000);
            float currentPosition = player.getCurrentPosition();
            int progress = (int) ((currentPosition / mediaFileLengthInMilliseconds) * 100);
            songSeekBar.setProgress(progress);

        } else if (v.getId() == R.id.backwardButton && player.isPlaying()) {
            player.seekTo(player.getCurrentPosition() - 10000);
            float currentPosition = player.getCurrentPosition();
            int progress = (int) ((currentPosition / mediaFileLengthInMilliseconds) * 100);
            songSeekBar.setProgress(progress);
        }

        return true;
    }

    private class PlayerThread extends HandlerThread {
        private MediaPlayer mPlayer;

        public PlayerThread(MediaPlayer player) throws IOException {
            super("PlayerThread");
            mPlayer = player;
            mPlayer.setDataSource(getContext(), Uri.parse(item.getMp3Url()));
            mPlayer.prepare();
        }

        @Override
        public void run() {
            mPlayer.start();
        }

        @Override
        public void interrupt() {
            if (mPlayer != null) {
                if (mPlayer.isPlaying()) {
                    mPlayer.stop();
                }
                mPlayer.release();
            }
            super.interrupt();
        }
    }

}
