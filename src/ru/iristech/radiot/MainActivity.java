package ru.iristech.radiot;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        RadiotFeedParser mw = new RadiotFeedParser();
        mw.start();

        ListView listView = (ListView)findViewById(R.id.listView);

        try {
            mw.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final List<Item> items = mw.getItems();

        MyAdapter adapter = new MyAdapter(this, items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PlayerDialog dialog = new PlayerDialog(MainActivity.this, true, null, items.get(position));
                dialog.show();
            }
        });

    }

    private class MyAdapter extends ArrayAdapter<Item> {
        private final Context context;
        private final List<Item> items;

        public MyAdapter(Context context, List<Item> items) {
            super(context, R.layout.row_layout, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_layout, parent, false);
            TextView textView = (TextView)rowView.findViewById(R.id.textView);
            TextView dateView = (TextView)rowView.findViewById(R.id.dateView);
            ImageView imageView = (ImageView)rowView.findViewById(R.id.imageView);
            Item item = getItem(position);
            if (item.getPicture() == null) {
                new ImageDownloader(imageView).execute(item);
            } else {
                imageView.setImageBitmap(item.getPicture());
            }
            textView.setText(item.getSubjects());
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.US);
            String date = format.format(item.getDate());
            dateView.setText(date);
            return rowView;
        }
    }

    private class ImageDownloader extends AsyncTask<Item, Void, Bitmap> {
        private static final String TAG = "ImageDownloader";
        ImageView imageView;

        private ImageDownloader(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Item... params) {
            InputStream in = null;
            try
            {
                URL url = new URL((params[0].getPictureUrl()));
                URLConnection urlConn = url.openConnection();
                HttpURLConnection httpConn = (HttpURLConnection) urlConn;
                httpConn.connect();
                in = httpConn.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Ошибка в ImageDownloader.doInBackground() при загрузке картинки: " + e);
            }
            Bitmap bitmap = BitmapFactory.decodeStream(in);
            params[0].setPicture(bitmap);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    }
}


































